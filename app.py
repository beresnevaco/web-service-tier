from flask import Flask, Response, jsonify, request, make_response
from flask_cors import CORS
import json
import datetime
import requests

# Local environment
# DataGen_Tier_ADDRESS='http://192.168.99.100:3005/'

# OpenShift
# DataGen_Tier_ADDRESS='https://tinyurl.com/yygfwxru'
# Dao_Tier_ADDRESS='https://tinyurl.com/y6edt6d8'
from auth_service import NotAuthorizedException, check_auth, create_token

DataGen_Tier_ADDRESS = 'http://datagen-casestudy.apps.dbgrads-6eec.openshiftworkshop.com/'
Dao_Tier_ADDRESS = 'http://dao-casestudy.apps.dbgrads-6eec.openshiftworkshop.com/'

app = Flask(__name__)
CORS(app)


@app.route('/')
def refer_to_accepted_format():
    return Response('''

        Supported Paths: \n 
        \n 
        /dao/user/<user_login> \n 
        /api/instrument/list \n
        /api/instrument/<instrument>/<start_date>/<end_date> \n
        /api/instrument/<instrument_name>/realtime \n
        /api/counterparty/stats/<start_date>/<end_date>
        ''', mimetype="text/plainText")


@app.route('/dao/user/<user_login>')
def get_user(user_login):
    # user = requests.get(f'{Dao_Tier_ADDRESS}dao/user/{user_login}')
    user = requests.get(f'http://dao-casestudy.apps.dbgrads-6eec.openshiftworkshop.com/dao/user/admin')
    print(user)
    return Response(user, mimetype="application/json")


@app.route('/dao/instrument/list')
def get_all_instruments():
    check_auth(request.headers.get('Authorization'))

    url = f'{Dao_Tier_ADDRESS}dao/instrument/{instrument_name}/{start_date}/{end_date}'
    result = requests.get(url)
    return Response(result, mimetype="application/json")


@app.route('/api/instrument/<instrument_name>/realtime')
def get_all_deals_for_one_instrument_realtime(instrument_name):
    r = requests.get(DataGen_Tier_ADDRESS, stream=True)

    def event_stream():

        for line in r.iter_lines():

            json_data = None

            if line:
                json_data = json.loads(line[5:])

            if json_data is not None and json_data["instrumentName"] == instrument_name:
                yield 'data: ' + str(json_data).replace('\'', '\"') + '\n\n'

    return Response(event_stream(), mimetype="text/event-stream")


@app.route('/api/instrument/<instrument_name>/<start_date>/<end_date>')
def get_all_deals_for_one_instrument(instrument_name, start_date, end_date):
    check_auth(request.headers.get('Authorization'))

    url = f'{Dao_Tier_ADDRESS}dao/instrument/{instrument_name}/{start_date}/{end_date}'
    r = requests.get(url)

    return_value = None
    for line in r.iter_lines():

        if line:
            return_value = line

    print(line)
    return Response(return_value, mimetype="application/json")


@app.route('/api/counterparty/stats/<start_date>/<end_date>')
def get_counterparty_stats(start_date, end_date):
    check_auth(request.headers.get('Authorization'))

    print(start_date)
    print(end_date)
    result = requests.get(f'{Dao_Tier_ADDRESS}dao/counterparty/stats/{start_date}/{end_date}')

    return Response(result, mimetype="application/json")


@app.route('/api/instrument/list')
def get_list_of_instruments():
    check_auth(request.headers.get('Authorization'))

    result = requests.get(f'{Dao_Tier_ADDRESS}dao/instrument/list')

    return Response(result, mimetype="application/json")


@app.route('/api/login', methods=['POST'])
def login():
    post_data = request.get_json()
    auth_token = create_token(post_data['user_login'], post_data['user_password'])
    responseObject = {
        'status': 'success',
        'auth_token': auth_token.decode()
    }
    return make_response(jsonify(responseObject)), 200


@app.errorhandler(Exception)
def handle_error(e):
    code = 401
    if isinstance(e, NotAuthorizedException):
        code = e.code
    return jsonify(error=str('Not Authorized')), code


if __name__ == "__main__":
    # Local
    # app.run("localhost", port=8080, debug=True)

    # Docker:
    app.run("0.0.0.0", port=8080, debug=True)

    # OpenShift:
    # app.run("0.0.0.0", port=8080, debug=True)





