# Dockerfile - this is a comment. Delete me if you want.
FROM python:3.7

COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt

EXPOSE 8080

ENTRYPOINT ["python"]
CMD ["./app.py"]