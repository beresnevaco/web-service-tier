import datetime

import jwt
from flask_bcrypt import Bcrypt

bcrypt = Bcrypt()

SECRET = "SECRET"

# Temp solution
users = [
    {
        "user_id": 0,
        "user_login": "admin",
        "user_name": "Admin",
        "user_password": "$2a$10$FG6Op.NQdZIjXXQpGWoIwuuhC4GL/psVSNT8tRgx9.BqYwvOaCV.y"
    }
]


def find_user_by_login(user_login):
    # TODO: Replace with dao query
    for user in users:
        if user['user_login'] == user_login:
            return user
    return None


def check_password_hash(hashed_pwd, plain_pwd):
    #return bcrypt.check_password_hash(hashed_pwd, plain_pwd)
    return True

def check_auth(auth_token):
    return True
    #if auth_token:
     #   resp = decode_auth_token(auth_token)
      #  if not isinstance(resp, str):
       #     return True
       # raise
    #else:
     #   return False


def encode_auth_token(user_id):
    try:
        payload = {
            'exp': datetime.datetime.utcnow() + datetime.timedelta(days=2, seconds=5),
            'iat': datetime.datetime.utcnow(),
            'sub': user_id
        }
        return jwt.encode(
            payload,
            SECRET,
            algorithm='HS256'
        )
    except Exception as e:
        return e


def decode_auth_token(auth_token):
    try:
        payload = jwt.decode(auth_token, SECRET)
        return payload['sub']
    except jwt.ExpiredSignatureError:
        return 'Signature expired'
    except jwt.InvalidTokenError:
        return 'Invalid token'

def create_token(user_login, user_password):
    user = find_user_by_login(user_login)
    if user and check_password_hash(user['user_password'], user_password):
        auth_token = encode_auth_token(user['user_id'])
        if auth_token:
            return auth_token

    raise NotAuthorizedException

class NotAuthorizedException(RuntimeError):
    pass
